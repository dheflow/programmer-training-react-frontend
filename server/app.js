const express = require('express');
const mysql = require('mysql');
const cors = require("cors");

const app = express();

app.use(express.json());
app.use(cors());

//create connection
const db = mysql.createConnection({
  host      : 'localhost',
  user      : 'root',
  password  : '0524139fairy',
  database  : 'DB_Rental'
})

//Connect 
db.connect((err) => {
  if(err) {
    console.log("[mysql error]",err);
  }
  console.log('MySQL Connected...');
})

//Add User
app.post("/addUserForm", (req,res) => {
  const name = req.body.name;
  const email = req.body.email;
  const username = req.body.username;
  const password = req.body.password;

  db.query(
    "INSERT INTO tb_users (name, email, username, password) VALUES (?,?,?,?)",
    [name, email, username, password],
    (err, result) => {
      console.log(err);
    }
  );
});

//Login
app.post("/login", (req,res) => {
  const username = req.body.username;
  const password = req.body.password;

  db.query(
    "SELECT * FROM tb_users WHERE username = ? AND password = ?",
    [username, password],
    (err, result) => {
      if(err) {
        res.send({ err: err });
      } 
      
      if (result.length > 0) {
        res.send(result);
      } else {
        res.send({ message: "Wrong username and password combination!" });
      }
    }
    
  );
});

//Transaction
app.get("/transaction", (req, res) => {
  db.query(`SELECT *, tb_customer.name AS customer_name_rental, tb_rental.price AS rental_price FROM tb_rental
  INNER JOIN tb_users ON tb_rental.users_id = tb_users.id
  INNER JOIN tb_customer ON tb_rental.customer_id = tb_customer.id
  INNER JOIN tb_motor ON tb_rental.motor_id = tb_motor.id
  INNER JOIN tb_vehicle_type ON tb_motor.vehicle_type_id = tb_vehicle_type.id
  `, (err, results, fields) => {
    if(err) throw err;
    res.send(JSON.stringify(results));
  });
});

//addTransactionForm
app.post("/addTransactionForm", (req, res) => {
  const transactionNumber = req.body.transactionNumber;
  const transactionDate = req.body.transactionDate;
  const userName = req.body.userName;
  const customerName = req.body.customerName;
  const motorName = req.body.motorName;
  const rentDuration = req.body.rentDuration;
  const price = req.body.price;

  db.query(
    `INSERT INTO tb_rental 
    (transaction_number, transaction_date, users_id, customer_id, motor_id, rent_duration, price) 
    VALUES (?,?,?,?,?,?,?)`,
    [transactionNumber, transactionDate, userName, customerName, motorName, rentDuration, price],
    (err, result) => {
      console.log(err);
    }
  );

  res.send(200, { message: 'ok' });
});

//addMotorForm
app.post("/addMotorForm", (req,res) => {
  const id = req.body.id
  const policeNumber = req.body.policeNumber;
  const vehicleTypeID = req.body.vehicleTypeID;
  const yearBuild = req.body.yearBuild;
  const yearOperated = req.body.yearOperated;
  const fuelType = req.body.fuelType;
  const ownerName = req.body.ownerName;

  db.query(
    `INSERT INTO tb_motor
    (id, police_number, vehicle_type_id, year_build, year_operated, fuel_type, owner_name)
    VALUES(?,?,?,?,?,?,?)`,
    [id, policeNumber, vehicleTypeID, yearBuild, yearOperated, fuelType, ownerName],
    (err, result) => {
      console.log(err);
    }
  );
  res.send(200, { massage : 'ok' });
})

//addCustomerForm
app.post("/addCustomerForm", (req, res) => {
  const id = req.body.id;
  const idNumber = req.body.idNumber;
  const name = req.body.name;
  const telephone = req.body.telephone;
  const address = req.body.address;
  const handphone = req.body.handphone;
  const email = req.body.email;

  db.query(
    `INSERT INTO tb_customer
    (id, id_number, name, telephone, address, handphone, email)
    VALUES(?,?,?,?,?,?,?)`,
    [id, idNumber, name, telephone, address, handphone, email],
    (err, result) => {
      console.log(err);
    }
  );
  res.send(200, { massage: 'ok' });
})

//addBrandForm
app.post("/addBrandForm", (req, res) => {
  const id = req.body.id;
  const name = req.body.name;
  const description = req.body.description;

  db.query(
    `INSERT INTO tb_brand
    (id, name, description)
    VALUES(?,?,?)`,
    [id, name, description],
    (err, result) => {
      console.log(err);
    }
  );
  res.send(200, { massage: 'ok' });
})

//UserList
app.get("/UserSelect", (req,res) => {
  db.query("SELECT * FROM tb_users", (err, results, fields) => {
    if(err) throw err;
    res.send(JSON.stringify(results));
  });
});

//CustomerList
app.get("/CustomerSelect", (req, res) => {
  db.query("SELECT * FROM tb_customer", (err, results, fields) => {
    if(err) throw err;
    res.send(JSON.stringify(results));
  });
})

//MotorList
app.get("/MotorSelect", (req, res) => {
  db.query("SELECT * FROM tb_motor", (err, results, fields) => {
    if(err) throw err;
    res.send(JSON.stringify(results));
  });
})

//BrandList
app.get("/BrandSelect", (req, res) => {
  db.query("SELECT * FROM tb_brand", (err, results, fields) => {
    if(err) throw err;
    res.send(JSON.stringify(results));
  });
})

//VehicleList
app.get("/VehicleSelect", (req, res) => {
  db.query("SELECT * FROM tb_vehicle_type", (err, results) => {
    if(err) throw err;
    res.send(JSON.stringify(results));
  })
})

//update
app.put("/update/:id", (req, res) => {
  const transactionNumber = req.body.transactionNumber;
  const transactionDate = req.body.transactionDate;
  const userName = req.body.userName;
  const customerName = req.body.customerName;
  const motorName = req.body.motorName;
  const rentDuration = req.body.rentDuration;
  const price = req.body.price;

  db.query(
    `UPDATE tb_rental SET transaction_number = ?, transaction_date = ?, users_id = ?, customer_id = ?, motor_id = ?, rent_duration = ?, price = ? WHERE id = ?`,
    [transactionNumber, transactionDate, userName, customerName, motorName, rentDuration, price, id],
    (err, results) => {
      if (err) {
        console.log(err);
      } else {
        res.send(JSON.stringify(results));
      }
    }
  );
});

//delete
app.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query("DELETE FROM tb_rental WHERE id = ?", id, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});


app.listen('3001', () => {
  console.log('server started on port 3001')
});