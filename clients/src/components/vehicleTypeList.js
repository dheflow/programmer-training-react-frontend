import React, { Component } from 'react';
import axios from 'axios';
import { SelectOption } from '../styles/Component.style';

export default class AddTransactionForm extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      vehicleTypeSelect: [],
      vehicleTypeList: []
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount() {
    axios.get("http://localhost:3001/vehicleSelect")
      .then(response => { 
        console.log(response); 
        this.setState({ vehicleTypeList: response.data, isLoading: false }) 
      }
    );
  }

  handleInputChange(value) {
    console.log(value);
    this.setState({ vehicleTypeSelect: value });    
  }
  

  render() {
    const { vehicleTypeList } = this.state;

    let vehicleListing = [];

    Array.from(vehicleTypeList).forEach((item, index) => {
      vehicleListing.push(
        <option
          key={index} 
          value={item.id}>
          {item.name}
        </option>
      )
    })

    var handleToUpdate = this.props.handleChangeVehicleList;

    return(
      <SelectOption className="selection-name">
        <select 
          defaultValue={this.state.vehicleTypeSelect.value}
          onChange={(e) => {this.handleInputChange(e.target.value); handleToUpdate(e.target.value)}}
        >
        {vehicleListing}
        </select>
      </SelectOption>
    )
  }
}