import React, { useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import axios from 'axios';
import { Button, AddTransactionPage, FormInputTransaction } from '../styles/Component.style';

export const AddBrand = () => {
  const history = useHistory();

  const [ id, setID ] = useState("");
  const [ name, setName ] = useState("");
  const [ description, setDescription ] = useState("");

  const addBrand = () => {
    axios({
      method: 'post',
      url: 'http://localhost:3001/addBrandForm',
      data: {
        id : id,
        name: name,
        description: description,
      }
    })
    .then(function (response) {
      history.push('/BrandPage');
      console.log(response);      
    })
    .catch(function (err) {
      console.log(err);
    });
  }

  return (
    <AddTransactionPage>
    <h1>Add Brand</h1>
    <FormInputTransaction>
      <div>
        <label>ID</label>
        <input
          type="text"
          onChange={(e) => {
            setID(e.target.value);
         }}  
        />
      </div>

      <div>
        <label>Name</label>
        <input
          type="text"
          onChange={(e) => {
            setName(e.target.value);
         }}  
        />
      </div>

      <div>
        <label>Description</label>
        <input
          type="text"
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        />
      </div>

      <div>
        <Button className="primary-button" onClick={addBrand}>Add Brand</Button>
      </div>

      <div>
        <Button className="danger-button">
          <Link to="/CustomerPage" >Cancel</Link>
        </Button>
      </div>

      </FormInputTransaction>
      
    </AddTransactionPage>
  )
}


export default AddBrand;