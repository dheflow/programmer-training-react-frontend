import React, { useState } from 'react';
import axios from 'axios';
import { Link, useHistory } from 'react-router-dom';
import { Button, AddTransactionPage, FormInputTransaction } from '../styles/Component.style';

export const AddUser = () => {
  const history = useHistory();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const addUser = () => {
    axios({
      method: 'post',
      url: 'http://localhost:3001/addUserForm',
      data: {
        name: name,
        email: email,
        username: username,
        password: password,
      }
    })
    .then(function (response) {
      history.push("/UserPage");
      console.log(response);      
    })
    .catch(function (err) {
      console.log(err);
    });
  }
  return (
    <AddTransactionPage>
    <h1>Add User</h1>
    <FormInputTransaction>
      <div>
        <label>Name</label>
        <input
          type="text"
          onChange={(e) => {
            setName(e.target.value);
         }}  
        />
      </div>
      <div>
        <label>Email</label>
        <input
          type="text"
          onChange={(e) => {
            setEmail(e.target.value);
         }}  
        />
      </div>

      <div>
        <label>Password</label>
        <input
          type="text"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />
      </div>

      <div>
        <label>Username</label>
          <input
            type="text"
            onChange={(e) => {
              setUsername(e.target.value);
            }}
          />  
      </div>

      <div>
        <Button className="primary-button" onClick={addUser}>Add User</Button>
      </div>

      <div>
        <Button className="danger-button">
          <Link to="/UserPage" >Cancel</Link>
        </Button>
      </div>

      </FormInputTransaction>
      
    </AddTransactionPage>
  )
};

export default AddUser;