import React, { useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import axios from 'axios';
import { Button, AddTransactionPage, FormInputTransaction } from '../styles/Component.style';

export const AddCustomer = () => {
  const history = useHistory();

  const [ idNumber, setIdNumber ] = useState("");
  const [ name, setName ] = useState("");
  const [ telephone, setTelephone ] = useState("");
  const [ address, setAddress ] = useState("");
  const [ handphone, setHandphone ] = useState("");
  const [ email, setEmail ] = useState("");
  const [ id, setID ] = useState("");

  const addCustomer = () => {
    axios({
      method: 'post',
      url: 'http://localhost:3001/addCustomerForm',
      data: {
        id : id,
        idNumber : idNumber,
        name: name,
        telephone: telephone,
        address: address,
        handphone: handphone,
        email: email,
      }
    })
    .then(function (response) {
      history.push('/CustomerPage');
      console.log(response);      
    })
    .catch(function (err) {
      console.log(err);
    });
  }

  return (
    <AddTransactionPage>
    <h1>Add Customer</h1>
    <FormInputTransaction>
      <div>
        <label>ID</label>
        <input
          type="text"
          onChange={(e) => {
            setID(e.target.value);
         }}  
        />
      </div>
      <div>
        <label>ID Number</label>
        <input
          type="text"
          onChange={(e) => {
            setIdNumber(e.target.value);
         }}  
        />
      </div>
      <div>
        <label>Name</label>
        <input
          type="text"
          onChange={(e) => {
            setName(e.target.value);
         }}  
        />
      </div>

      <div>
        <label>Telephone</label>
        <input
          type="text"
          onChange={(e) => {
            setTelephone(e.target.value);
          }}
        />
      </div>

      <div>
        <label>Address</label>
          <input
            type="text"
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          />  
      </div>

      <div>
        <label>Handphone</label>
        <input
          type="text"
          onChange={(e) => {
            setHandphone(e.target.value);
          }}
        />
      </div>

      <div>
        <label>Email</label>
        <input
          type="text"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
      </div>

      <div>
        <Button className="primary-button" onClick={addCustomer}>Add Customer</Button>
      </div>

      <div>
        <Button className="danger-button">
          <Link to="/CustomerPage" >Cancel</Link>
        </Button>
      </div>

      </FormInputTransaction>
      
    </AddTransactionPage>
  )
}


export default AddCustomer;