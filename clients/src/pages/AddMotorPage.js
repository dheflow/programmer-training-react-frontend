import React, { useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import axios from 'axios';
import { Button, AddTransactionPage, FormInputTransaction } from '../styles/Component.style';
import VehicleTypeList from '../components/vehicleTypeList';

export const AddMotor = () => {
  const history = useHistory();

  const [ id, setID ] = useState("");
  const [ policeNumber, setPoliceNumber ] = useState("");
  const [ vehicleTypeID, setVehicleTypeID ] = useState("");
  const [ yearBuild, setYearBuild ] = useState("");
  const [ yearOperated, setYearOperated ] = useState("");
  const [ fuelType, setFuelType ] = useState("");
  const [ ownerName, setOwnerName ] = useState("");


  const addMotor = () => {
    axios({
      method: 'post',
      url: 'http://localhost:3001/addMotorForm',
      data: {
        id : id,
        policeNumber: policeNumber,
        vehicleTypeID: vehicleTypeID,
        yearBuild: yearBuild,
        yearOperated: yearOperated,
        fuelType: fuelType,
        ownerName : ownerName,
      }
    })
    .then(function (response) {
      history.push('/MotorPage');
      console.log(response);      
    })
    .catch(function (err) {
      console.log(err);
    });
  }

  const handleChangeVehicle = (value) => {
    setVehicleTypeID(value);  
  }

  return (
    <AddTransactionPage>
    <h1>Add Motor</h1>
    <FormInputTransaction>
      <div>
        <label>ID</label>
        <input
          type="text"
          onChange={(e) => {
            setID(e.target.value);
         }}  
        />
      </div>
      <div>
        <label>Police Number</label>
        <input
          type="text"
          onChange={(e) => {
            setPoliceNumber(e.target.value);
         }}  
        />
      </div>

      <div>
        <label>Vehicle Type ID</label>
        <VehicleTypeList
          handleChangeVehicleList={handleChangeVehicle}
        />
      </div>

      <div>
        <label>Year Build</label>
        <input
          type="text"
          onChange={(e) => {
            setYearBuild(e.target.value);
          }}
        />
      </div>

      <div>
        <label>Year Operated</label>
          <input
            type="text"
            onChange={(e) => {
              setYearOperated(e.target.value);
            }}
          />  
      </div>

      <div>
        <label>Fuel Type</label>
        <input
          type="text"
          onChange={(e) => {
            setFuelType(e.target.value);
          }}
        />
      </div>

      <div>
        <label>Owner Name</label>
        <input
          type="text"
          onChange={(e) => {
            setOwnerName(e.target.value);
          }}
        />
      </div>

      <div>
        <Button className="primary-button" onClick={addMotor}>Add Motor</Button>
      </div>

      <div>
        <Button className="danger-button">
          <Link to="/MotorPage" >Cancel</Link>
        </Button>
      </div>

      </FormInputTransaction>
      
    </AddTransactionPage>
  )
}


export default AddMotor;