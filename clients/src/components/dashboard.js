import React, { useState } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import { Button, Tbody, LayoutPage, Table, Thead } from '../styles/Component.style';

function Dashboard() {

  const [ transactionList, setTransactionList ] = useState([]);

  const getTransaction = () => {
    axios.get("http://localhost:3001/transaction").then((response) => {
      setTransactionList(response.data)
    })
  };

  const deleteTransaction = (id) => {
    axios.delete(`http://localhost:3001/delete/${id}`).then((response) => {
      setTransactionList(
        transactionList.filter((item) => {
          return item.id !== id;
        })
      );
    });
    console.log(id);
  }

  function WithoutTime(dateTime) {
    const date = new Date(dateTime);
    return Intl.DateTimeFormat('en-US', { dateStyle: 'full' }).format(date);
  }

  return (
    <LayoutPage>
        <div className="top-section">
          <h1>Transaction</h1>
          <div className="add-data">
            <Button className="green-button">
              <Link to="/AddTransactionPage" className="add-button">Add Transaction</Link>
            </Button>
          </div>
        </div>
        <Table>
          <Thead>
            <tr>
              <th>Number</th>
              <th>Date</th>
              <th>Username</th>
              <th>Costumer</th>
              <th>Motor</th>
              <th>Duration</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </Thead>
          <Tbody>
          {transactionList.map((item, index) => {
            return (
              <tr key={item.transaction_number}>
                <td>{item.transaction_number}</td>
                <td>{WithoutTime(item.transaction_date)}</td>
                <td>{item.username}</td>
                <td>{item.customer_name_rental}</td>
                <td>{item.name}</td>
                <td>{item.rent_duration} Days</td>
                <td>Rp. {item.rental_price}</td>
                <td>
                  <div style={buttonLinier}>
                  <Button className="primary-button">
                    <Link to="/UpdateTransactionPage" className="medium-button">Update</Link>
                  </Button>
                  <Button onClick={() => {deleteTransaction(item.id)}} className="danger-button">
                    <div className="medium-button">
                      Delete
                    </div>
                  </Button>
                  </div>
                </td>
              </tr>
            )
          })}
          </Tbody>
        </Table>
        <Button onClick={getTransaction} className="Show-Button primary-button">Show Transaction</Button>
      </LayoutPage>
  )

}

export const buttonLinier = {
  display: 'flex'
}

export default Dashboard;